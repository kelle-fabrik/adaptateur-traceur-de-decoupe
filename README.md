Adaptateur lame pour traceur GRAPHTEC 
=============================


Réalisation d'un adaptateur pour un support base de lame sur un traceur **GRAPHTEC cutting pro FC3100-120**

| <img src="Images/IMG_20191023_134628407.jpg" alt="drawing" width="400"/> | <img src="Images/IMG_20191023_134708105.jpg" alt="drawing" width="400"/> |


**Machine nécessaire**

* Imprimante 3D

**Matériel utilisé**

* Filament PLA

**Logiciel utilisé**

* FreeCAD

**Fichiers à disposition**

* [FCStd](Fichiers/support_coupe.FCStd)
* [stl](Fichiers/support_coupe.stl)

Procédés
------

1. Modélisation du support 3D
2. Impression 3D

> Site internet : [Kelle Fabrik](https://kellefabrik.org/)


